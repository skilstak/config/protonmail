# ProtonMail

*[This repo is mirrored from ![Fox](gitlab.png) GitLab (a superior Git hosting platform).](https://gitlab.com/skilstak/config/protonmail)*

No other email services matches ProtonMail.

When using bridge also get offlineimap setup (which unfortunately is a Python application until some generous soul ports it to Go).
